import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LoginModel} from './login.model';
import {map} from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  readonly TOKEN = 'TOKEN';
  constructor(private httpClient: HttpClient) { }

  public login(pEmail, pPassword): Observable<LoginModel> {
    return this.httpClient.post(`${environment.apiHost}/login`, {
      'email': pEmail,
      'password': pPassword
    }).pipe( map( (res: LoginModel) => {
      this.storeToLS(res);
      return res;
    }));
  }
  public logout() {
   localStorage.removeItem(this.TOKEN);
  }
  private storeToLS(res: LoginModel) {
    localStorage.setItem(this.TOKEN, res.email);
  }
  public isLoggedIn(): boolean {
    return localStorage.getItem(this.TOKEN) !== null;
  }
}
