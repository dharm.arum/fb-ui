import { Component, OnInit } from '@angular/core';
import {LoginService} from './login.service';
import {LoginModel} from './login.model';
import {Router} from '@angular/router';
import {MatDialog, MatSnackBar} from '@angular/material';
import {delay} from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginModel: LoginModel = new LoginModel();
  public isLoading = false;
  constructor(private snapBar: MatSnackBar, private loginService: LoginService, private router: Router, private dialog: MatDialog) { }

  ngOnInit() {
  }

  public login() {
    /*
    this.dialog.open(ErrorComponent,{
      data: {
        message:  'Your login information are incorrect!'
    }});*/
    this.isLoading = true;
    this.loginService.login(this.loginModel.email, this.loginModel.password).subscribe( (res: LoginModel) => {
      this.router.navigateByUrl('/attendance');
      this.isLoading = false;
    }, (err) => {
      this.isLoading = false;
      this.loginModel.email = '';
      this.loginModel.password = '';
      this.snapBar.open(err && err.error.message, '', {
        duration: 4000
      });
    });
  }
}
