export class LoginModel {
    public email: string;
    public password: string;
    public name: string;
    public enabled: number;
    public address: string;
}
