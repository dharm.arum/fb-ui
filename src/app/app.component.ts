import { Component } from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from './login/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'flybounce-ui';
  constructor(private router: Router, public loginService: LoginService) {}

  public onLogOut() {
    this.loginService.logout();
    this.router.navigateByUrl('/login');
  }
}
