export class User {
  public name: string;
  public email: string;
  public address: string;
  public role: string;
  public enabled: Number;
}
