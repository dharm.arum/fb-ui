import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {CustomMaterialModule} from './core/material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoginService} from './login/login.service';
import {HttpClientModule} from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import {AuthGaurd} from './login/AuthGaurd';
import { AttendanceComponent } from './attendance/attendance.component';
import {MatAutocompleteModule, MatCheckboxModule, MatSelectModule} from '@angular/material';
import { ErrorComponent } from './error/error.component';
import { UsersComponent } from './users/users.component';
import { LoaderComponent } from './shared/loader/loader.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AttendanceComponent,
    ErrorComponent,
    UsersComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    CustomMaterialModule,
    HttpClientModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatSelectModule
  ],
  entryComponents: [
    ErrorComponent
  ],
  providers: [
    LoginService,
    AuthGaurd
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
