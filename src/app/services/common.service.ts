import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../users/user.model';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private httpClient: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.httpClient.get(`${environment.apiHost}/users`)
    .pipe(
      map( (res: User[]) => {
        return res;
      })
    );
  }
}
