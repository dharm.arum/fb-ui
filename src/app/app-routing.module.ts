import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {AuthGaurd} from './login/AuthGaurd';
import {HomeComponent} from './home/home.component';
import {AttendanceComponent} from './attendance/attendance.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {path : '', component : LoginComponent},
  {
    path: 'dashboard',
    component: HomeComponent,
    canActivate: [AuthGaurd] // <---- connected Route with guard
  },
  { path: 'attendance', component: AttendanceComponent, canActivate: [AuthGaurd] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
