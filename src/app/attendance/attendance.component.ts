import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {delay, map, startWith} from 'rxjs/operators';
import {CommonService} from '../services/common.service';
import {User} from '../users/user.model';
import {AttendanceService} from './attendance.service';
import {ErrorStateMatcher, MatSnackBar} from '@angular/material';

/** Error when the parent is invalid */
class CrossFieldErrorMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return control.dirty && form.invalid;
  }
}

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.scss']
})
export class AttendanceComponent implements OnInit {
  attendanceForm: FormGroup;
  filteredUsers: Observable<{}[]>;
  coachingTypes: {}[] = [{id: 0, value: 'Regular'}, {id: 1, value: 'Special'}, {id: 2, value: 'Adhoc'}];
  users: User[];
  isLoading = false;
  errorMatcher = new CrossFieldErrorMatcher();
  //
  constructor(private commonService: CommonService, private attendanceService: AttendanceService,
              private fb: FormBuilder, private snackBar: MatSnackBar) {
    this.attendanceForm = fb.group({
      'user': [''],
      'noclass': [false],
      'noclassreason': [''],
      'isabsent': [false],
      'startdate': [''],
      'enddate': [''],
      'coachingtype': ['']
    }, {
      validators: [this.attendanceValidatorFn]
    });
    this.attendanceForm.controls['noclassreason'].disable();
  }

  attendanceValidatorFn(control: AbstractControl): { [key: string]: boolean } | null {
    if (control.value['noclass'] && !control.value['noclassreason']) {
      return {
        'noreasonmissing': true
      };
    }
    if (!control.value['noclass']) {
      if (!control.value['user']) {
        return {
          'usermissing': true
        };
      }
      if (!control.value['coachingtype']) {
        return {
          'coachingtypemissing': true
        };
      }
      if (!control.value['isabsent']) {
        if (!control.value['startdate']) {
          return {
            'startdatemissing': true
          };
        }
        if (!control.value['enddate']) {
          return {
            'enddatemissing': true
          };
        }
        if (control.value['startdate']) {
          const timeRegexInHHMM: RegExp = new RegExp('^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]$');
          if (!timeRegexInHHMM.test(control.value['startdate'])) {
            return {
              'startdateinvalid': true
            };
          }
        }
        if (control.value['enddate']) {
          const timeRegexInHHMM: RegExp = new RegExp('^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]$');
          if (!timeRegexInHHMM.test(control.value['enddate'])) {
            return {
              'enddateinvalid': true
            };
          }
        }

        if (control.value['startdate'] && control.value['enddate']) {
          const startDateInMillis: Date = new Date(),
                endDateInMillis: Date = new Date();
          //
          startDateInMillis.setHours(parseInt(control.value['startdate'].substring(0, 2), 10));
          startDateInMillis.setMinutes(parseInt(control.value['startdate'].substring(3), 10));
          endDateInMillis.setHours(parseInt(control.value['enddate'].substring(0, 2), 10));
          endDateInMillis.setMinutes(parseInt(control.value['enddate'].substring(3), 10));
          //
          if (startDateInMillis.getTime() > endDateInMillis.getTime()) {
            return {
              'stardategreatethanenddate': true
            };
          }
          if (startDateInMillis.getTime() === endDateInMillis.getTime()) {
            return {
              'stardatesameasenddate': true
            };
          }
        }
      }
    }
    return null;
  }
  ngOnInit() {
    //
    this.isLoading = true;
    this.commonService.getUsers().subscribe( (res: User[]) => {
      this.users = res;
      this.filteredUsers = this.attendanceForm.controls['user'].valueChanges
        .pipe(
          startWith({}),
          delay(0),
          map(value => {
            this.isLoading = false;
            return this._filter(value);
          })
        );
    });
    //
  }

  private _filter(value: any): {}[] {
    const returnArr = this.users.filter((user: User) => {
      return user.role.toLowerCase() === 'student' && user.enabled === 1;
    });
    return returnArr;
  }

  onSubmit() {
    this.isLoading = true;
    const attendanceData = this.attendanceForm.value;
    const starttime: string = attendanceData['startdate'];
    const endtime: string = attendanceData['enddate'];
    //
    const startDateInMillis = new Date();
    const endDateInMillis = new Date();
    //
    if (!attendanceData['isabsent'] || attendanceData['isabsent'] === false || !attendanceData['noclass'] || attendanceData['noclass'] === false) {
      starttime && startDateInMillis.setHours(parseInt(starttime.substring(0, 2), 10));
      starttime && startDateInMillis.setMinutes(parseInt(starttime.substring(3), 10));
      //
      endtime && endDateInMillis.setHours(parseInt(endtime.substring(0, 2), 10));
      endtime && endDateInMillis.setMinutes(parseInt(endtime.substring(3), 10));
      //
      attendanceData['startdate'] = startDateInMillis && startDateInMillis.getTime();
      attendanceData['enddate'] = endDateInMillis && endDateInMillis.getTime();
    }
    if (!attendanceData['noclass'] || attendanceData['noclass'] === false) {
      attendanceData['userid'] = attendanceData['user'] && attendanceData['user']['id'];
      attendanceData['username'] = attendanceData['user'] && attendanceData['user']['name'];
      attendanceData['coachingtype'] = attendanceData['coachingtype'] && attendanceData['coachingtype']['value'];
    }
    //
    this.attendanceService.saveAttendance(attendanceData).subscribe( (res) => {
      this.isLoading = false;
      this.snackBar.open('Attendance saved successully', '', {
        duration: 4000
      });
      this.attendanceForm.reset();
    }, () => {
      this.snackBar.open('Error in saving the attendance', '', {
        duration: 4000
      });
    });
  }

  onNoClassChange() {
    if (this.attendanceForm.controls['noclass'].value === true) {
      this.attendanceForm.controls['noclassreason'].enable();
      this.attendanceForm.controls['user'].disable();
      this.attendanceForm.controls['coachingtype'].disable();
      this.attendanceForm.controls['isabsent'].disable();
      this.attendanceForm.controls['startdate'].disable();
      this.attendanceForm.controls['enddate'].disable();
    } else {
      this.attendanceForm.controls['noclassreason'].disable();
      this.attendanceForm.controls['user'].enable();
      this.attendanceForm.controls['coachingtype'].enable();
      this.attendanceForm.controls['isabsent'].enable();
      this.attendanceForm.controls['startdate'].enable();
      this.attendanceForm.controls['enddate'].enable();
    }
  }
  onIsAbsentChange() {
    if (this.attendanceForm.controls['isabsent'].value === true) {
      this.attendanceForm.controls['startdate'].disable();
      this.attendanceForm.controls['enddate'].disable();
    } else {
      this.attendanceForm.controls['startdate'].enable();
      this.attendanceForm.controls['enddate'].enable();
    }
  }
}
