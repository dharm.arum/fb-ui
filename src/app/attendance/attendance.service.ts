import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AttendanceService {

  constructor(private httpClient: HttpClient) { }

  saveAttendance(postData: any) {
    return this.httpClient.post(`${environment.apiHost}/attendance`, postData, {
      'headers': {
        'content-type': 'application/json'
      }
    }).pipe( map((res) => {
      return res;
    }));
  }
}
